<?php
namespace Process;
use FTPClient\FTPClient;
use FTPClient\Traits\SortStrategyTrait;

/**
 * Process Custom
 *
 * @author Lukas Skywalker <lukas@paiskr.eu>
 */
class CustomProcess
{
    /**
     * SortStrategy Trait
     */
    use SortStrategyTrait;
    /**
     * @var string
     */
    protected $_serverString;
    /**
     * @var string
     */
    protected $_baseFolder;
    /**
     * @var \FTPClient\FTPClient
     */
    protected $_ftpClient;
    /**
     * Process CustomProcess
     */
    public function __construct($server, $name, $password, $baseFolder = '')
    {
        if(is_null($this->_getFTPClient()))
            $this->_setFTPClient(new FTPClient($server,$name,$password));

        $this->setServerString($server);
        $this->setBaseFolder($baseFolder);
    }
    /**
     * Executing Custom Process
     */
    public function execute()
    {
        //Everythink you want
        return true;
    }


    /************************************** [ Getters & Setters ] *****************************************************/
    /**
     * Returns FTP Client
     * @return \FTPClient\FTPClient
     */
    protected function _getFTPClient()
    {
        return $this->_ftpClient;
    }
    /**
     * Setting FTP Client
     * @param \FTPClient\FTPClient $ftpClient
     */
    protected function _setFTPClient(\FTPClient\FTPClient $ftpClient)
    {
        $this->_ftpClient = $ftpClient;
    }
    /**
     * @return string
     */
    public function getServerString()
    {
        return $this->_serverString;
    }
    /**
     * @param string $server
     */
    public function setServerString($server)
    {
        $this->_serverString = $server;
    }
    /**
     * @return string
     */
    public function getBaseFolder()
    {
        return $this->_baseFolder;
    }
    /**
     * @param string $baseFolder
     */
    public function setBaseFolder($baseFolder)
    {
        $this->_baseFolder = $baseFolder;
    }
}