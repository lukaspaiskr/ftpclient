<?php
namespace FTPClient\Traits;
/**
 * SortStrategyTrait
 * Inspired by www.php.net
 *
 * @author Lukas Skywalker <lukas@paiskr.eu>
 */
trait SortStrategyTrait
{
    /**
     * Setting which the field to be compared
     * @var null
     */
    protected $sort_field = null;
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function string_asc($item1, $item2)
    {
        return strnatcmp($item1[$this->sort_field], $item2[$this->sort_field]);
    }
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function string_desc($item1, $item2)
    {
        return strnatcmp($item2[$this->sort_field], $item1[$this->sort_field]);
    }
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function num_asc($item1, $item2)
    {
        if ($item1[$this->sort_field] == $item2[$this->sort_field])
            return 0;

        return ($item1[$this->sort_field] < $item2[$this->sort_field] ? -1 : 1 );
    }
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function num_desc($item1, $item2)
    {
        if ($item1[$this->sort_field] == $item2[$this->sort_field])
            return 0;

        return ($item1[$this->sort_field] > $item2[$this->sort_field] ? -1 : 1 );
    }
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function date_asc($item1, $item2)
    {
        $date1 = intval(str_replace('-', '', $item1[$this->sort_field]));
        $date2 = intval(str_replace('-', '', $item2[$this->sort_field]));

        if ($date1 == $date2)
            return 0;

        return ($date1 < $date2 ? -1 : 1 );
    }
    /**
     * @param $item1
     * @param $item2
     * @return int
     */
    protected function date_desc($item1, $item2)
    {
        $date1 = intval(str_replace('-', '', $item1[$this->sort_field]));
        $date2 = intval(str_replace('-', '', $item2[$this->sort_field]));

        if ($date1 == $date2)
            return 0;

        return ($date1 > $date2 ? -1 : 1 );
    }
}