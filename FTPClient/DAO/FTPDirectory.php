<?php
namespace FTPClient\DAO;
/**
 * FTP Directory Data Attribute Object
 *
 * @author Lukas Skywalker <lukas@paiskr.eu>
 */

class FTPDirectory extends FTPObject
{
    /**
     * @var null|FTPObject[]
     */
    public $Children;
    /**
     * FTP Data Attribute Object
     *
     * @param null|string $Name
     * @param null|string $Path
     * @param null|string $Size
     * @param null|string $Chmod
     * @param null|string $Date
     * @param null|string $Children
     */
    public function __construct($Name = NULL, $Path = NULL, $Size = NULL, $Chmod = NULL, $Date = NULL, $Children = NULL)
    {
        parent::__construct($Name,$Path, $Size, $Chmod, $Date);
        $this->Children = $Children;
    }
}