<?php
namespace FTPClient\DAO;
/**
 * Abstract FTP Data Attribute Object
 *
 * @author Lukas Skywalker <lukas@paiskr.eu>
 */

abstract class FTPObject implements \IteratorAggregate, \ArrayAccess
{
    /**
     * @var string
     */
    public $Name;
    /**
     * @var string
     */
    public $Path;
    /**
     * @var string
     */
    public $Size;
    /**
     * @var string
     */
    public $Chmod;
    /**
     * @var string
     */
    public $Date;

    /**
     * FTP Data Attribute Object
     *
     * @param null|string $Name
     * @param null|string $Path
     * @param null|string $Size
     * @param null|string $Chmod
     * @param null|string $Date
     */
    public function __construct($Name = NULL, $Path = NULL, $Size = NULL, $Chmod = NULL, $Date = NULL)
    {
        $this->Name = $Name;
        $this->Path = $Path;
        $this->Size = $Size;
        $this->Chmod = $Chmod;
        $this->Date = $Date;
    }

    /**
     * @param $key
     * @return bool
     */
    public function keyExists($key)
    {
        return array_key_exists($key, get_object_vars($this));
    }

    /**
     * @param $key
     * @return null
     */
    public function getValueByKey($key)
    {
        if(!$this->keyExists($key))
        {
            return null;
        }
        return get_object_vars($this)[$key];
    }

    /**
     * @param $key
     * @param $value
     */
    public function setValueByKey($key, $value)
    {
        if(!$this->keyExists($key))
        {
            throw new \InvalidArgumentException('Key ['.$key.'] does not exist!');
            return;
        }
        else
        {
            $data = get_object_vars($this);
            $data[$key] = $value;
        }
    }

    /**
     * @param $key
     */
    public function unsetByKey($key)
    {
        $this->setValueByKey($key, NULL);

    }
    //------------ IteratorAggregate ------------//
    /**
     * @return \ArrayIterator
     */
    public function getIterator()
    {
        return new \ArrayIterator(get_object_vars($this));
    }
    public function __get($key)
    {
        return $this->getValueByKey($key);
    }
    public function __set($key, $value)
    {
        $this->setValueByKey($key, $value);
    }
    public function __isset($key)
    {
        return $this->keyExists($key);
    }
    //------------- ArrayAccess ----------------//
    public function offsetExists($index)
    {
        return $this->keyExists($index);
    }
    public function offsetGet($index)
    {
        return $this->getValueByKey($index);
    }
    public function offsetSet($offset, $value)
    {
        $this->setValueByKey($offset, $value);
    }
    public function offsetUnset($offset)
    {
        $this->unsetByKey($offset);
    }
}