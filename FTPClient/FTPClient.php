<?php
namespace FTPClient;
use FTPClient\DAO\FTPDirectory;
use FTPClient\DAO\FTPFile;
use FTPClient\Traits\SortStrategyTrait;

/**
 * Simply FTP Client
 *
 * @author Lukas Skywalker <lukas@paiskr.eu>
 */
class FTPClient
{
    use SortStrategyTrait;
    /**
     * Constants
     */
    const MODE_ASCII = FTP_ASCII;
    const MODE_TEXT = FTP_TEXT;
    const MODE_BINARY = FTP_BINARY;
    const MODE_IMAGE = FTP_IMAGE;
    const TIMEOUT_SEC = FTP_TIMEOUT_SEC; //See <b>ftp_set_option</b> for information.
    const AUTO_SEEK = FTP_AUTOSEEK; //See <b>ftp_set_option</b> for information.
    /**
     * Automatically determine resume position and start position for GET and PUT requests
     * (only works if FTP_AUTOSEEK is enabled)
     */
    const AUTO_RESUME = FTP_AUTORESUME;
    const STATUS_FAILED = FTP_FAILED; //Asynchronous transfer has failed
    const STATUS_FINISHED = FTP_FINISHED; //Asynchronous transfer has finished
    const MORE_DATA = FTP_MOREDATA; //Asynchronous transfer is still active
    /**
     * FTP Connection resource
     * @var resource ftp_connect
     */
    protected $_connection;
    /**
     * FTP Login Username
     * @var string
     */
    protected $_username;
    /**
     * FTP Login Password
     * @var string
     */
    protected $_password;
    /**
     * FTP server URL
     * @var string
     */
    protected $_ftpServer;
    /**
     * @var bool
     */
    protected $_passive_mode = true;
    /**
     * FTP Client
     * setting basic options
     *
     * @param string $ftp_server
     * @param string $username
     * @param string $password
     */
    public function __construct($ftp_server, $username = "anonymous", $password = "anonymous@test.cz")
    {
        $this->setFtpServer($ftp_server);
        $this->setUsername($username);
        $this->setPassword($password);
    }
    /**
     * Connection to server
     *
     * @param boolean $ssl
     * @throws \FTPClient\FTPClientException
     * @return resource
     */
    public function connect($ssl = false)
    {
        if(is_null($this->_getConnection()))
        {
            if(!$ssl)
                $connection = ftp_connect($this->_getFtpServer());
            else
                $connection = ftp_ssl_connect($this->_getFtpServer());

            if(!$connection)
            {
                throw new \FTPClient\FTPClientException('Failed to open connection to server');
                return;
            }

            if(!ftp_login($connection, $this->_getUsername(), $this->_getPassword()))
            {
                throw new \FTPClient\FTPClientException('Failed login to server');
                return;
            }

            ftp_pasv($connection, $this->isPassiveMode());

            $this->setConnection($connection);
        }

        return $this->_getConnection();
    }
    /**
     * Returns actual path on ftp
     * @return string
     */
    public function getActualPath()
    {
        return ftp_pwd($this->_getConnection());
    }
    /**
     * Screening folder from path
     *
     * @param string $path
     * @param bool $recursive
     * @return \FTPClient\DAO\FTPObject[]
     */
    public function screenFolder($path, $recursive = TRUE)
    {
        ftp_chdir($this->_getConnection(), $path);
        $contents = ftp_rawlist($this->_getConnection(), '.', $recursive);

        return $this->_buildTree($contents);
    }
    /**
     * Searching files in folders
     * @param string $path
     * @param array $extension
     * @param string $sort
     */
    public function findFiles($path, array $extension = array(), $sort = 'date_desc')
    {
        $tree = $this->screenFolder($path);
        $files = $this->_multidimensionalSearch($tree);
        $output = array();
        foreach($files as $file)
        {
            $fileInfo = pathinfo($file->Name);
            if((count($extension)>0) && in_array($fileInfo['extension'],$extension))
                $output[] = $file;
            elseif (count($extension) == 0)
                $output[] = $file;
        }

        switch($sort)
        {
            default:
                $this->_sort_field = 'Date';
                uasort($output, array($this, 'date_desc'));
            break;
        }

        return $output;
    }
    /**
     * Creating Directory on server
     *
     * @param string $dir
     * @throws FTPClientException
     * @return bool
     */
    public function createDir($dir)
    {
        if(!ftp_mkdir($this->_getConnection(), $dir))
        {
            throw new FTPClientException('Trying to create directory was failed.');
            return false;
        }

        return true;
    }
    /**
     * Remove Directory on server
     * @param string $dir
     * @return bool
     * @throws FTPClientException
     */
    public function removeDir($dir)
    {
        if (!ftp_rmdir($this->_getConnection(), $dir)) {
            throw new FTPClientException('Trying to remove directory was failed.');
            return false;
        }

        return true;
    }
    /**
     * Upload file to server
     * @param $tmp_file
     * @param string $path_to_file
     * @param null|string $filename
     * @param self::MODE_* $mode
     * @return bool|string
     * @throws FTPClientException
     */
    public function uploadFile($tmp_file, $path_to_file, $filename = NULL, $mode = self::MODE_BINARY)
    {
        $destination_file = is_null($filename) ? $path_to_file : $path_to_file . $filename;
        $upload = ftp_put($this->_getConnection(), $destination_file, $tmp_file, $mode);
        if (!$upload)
        {
            throw new FTPClientException("There was a problem while uploading $destination_file\n");
            return false;
        }
        return $destination_file;

    }
    /**
     * Download file from server
     * @param string $path_to_file
     * @param null|string $saveAs
     * @param int $mode
     * @return bool
     * @throws FTPClientException
     */
    public function downloadFile($path_to_file, $saveAs = NULL, $mode = self::MODE_BINARY)
    {
        $local_file = is_null($saveAs) ? pathinfo($path_to_file)['basename'] : $saveAs;
        if (!ftp_get($this->_getConnection(), $local_file, $path_to_file, $mode))
        {
            throw new FTPClientException("Trying to download file from server was failed.\n");
            return false;
        }
        return true;
    }
    /**
     * Change file permissions
     *
     * @param string $filename
     * @param int|string $permission_mode
     * @return bool
     * @throws FTPClientException
     */
    public function changeFilePermission($filename, $permission_mode = 0644)
    {
        if (!ftp_chmod($this->_getConnection(),$permission_mode, $filename)) {
            throw new FTPClientException('Trying to change file permissions was failed.');
            return false;
        }

        return true;
    }

    /**
     * Delete file from server
     * @param string $path
     * @return bool
     * @throws FTPClientException
     */
    public function deleteFile($path)
    {
        if (!ftp_delete($this->_getConnection(), $path))
        {
            throw new FTPClientException('Trying to delete file was failed.');
            return false;
        }

        return true;
    }
    /**
     * Set miscellaneous runtime FTP options
     * self::TMEOUT_SEC | FTP_TIMEOUT_SEC	Changes the timeout in seconds used for all network related functions.
     * value must be an integer that is greater than 0. The default timeout is 90 seconds.
     * self::AUTO_SEEK | FTP_AUTOSEEK	When enabled, GET or PUT requests with a resumepos or startpos parameter
     * will first seek to the requested position within the file. This is enabled by default.
     *
     * @param int $option
     * @param int|string $value
     * @return bool
     * @throws FTPClientException
     */
    public function setOption($option, $value)
    {
        if(!ftp_set_option($this->_getConnection(), $option, $value))
        {
            throw new FTPClientException('Try to set option was failed.');
            return false;
        }

        return true;
    }
    /**
     * Retrieves various runtime behaviours of the current FTP stream
     * Supported runtime FTP options
     * self::TIMEOUT_SEC | FTP_TIMEOUT_SEC	Returns the current timeout used for network related operations.
     * self::AUTO_SEEK | FTP_AUTOSEEK	Returns TRUE if this option is on, FALSE otherwise.
     * @param $option
     * @return bool|mixed
     * @throws FTPClientException
     */
    public function getOption($option)
    {
        $optionValue = ftp_get_option($this->_getConnection(), $option);
        if(!$optionValue)
        {
            throw new FTPClientException('Option ['.$option.'] not supported.');
            return false;
        }
        return $optionValue;
    }
    /**
     * Searching files in folders and subfolders
     * @param \FTPClient\DAO\FTPObject[] $files
     * @return \FTPClient\DAO\FTPObject[]
     */
    protected function _multidimensionalSearch(array $files)
    {
        $output = array();
        foreach($files as $file)
        {

            if($file instanceof \FTPClient\DAO\FTPDirectory)
            {
                /** @var \FTPClient\DAO\FTPDirectory $file */
                if(!is_null($file->Children))
                    $output = array_merge($output,$this->_multidimensionalSearch($file->Children));
            }
            else
                $output[] = $file;
        }

        return $output;
    }
    /**
     * Build array tree from ftpRawFiles
     * @param array $rawFiles
     * @return \FTPClient\DAO\FTPObject[]
     */
    protected function _buildTree(array $rawFiles)
    {
        $structure = array();
        $arrayPointer = &$structure;
        $filePath = $this->getActualPath();

        foreach ($rawFiles as $rawFile)
        {
            if (preg_match("/(\s)?\:$/", $rawFile))
            {
                $paths = explode('/', str_replace(':', '', $rawFile));
                $arrayPointer = &$structure;
                foreach ($paths as $path)
                {
                    foreach ($arrayPointer as $i => $file)
                    {
                        /**
                         * @var \FTPClient\DAO\FTPObject $file
                         */
                        if (($file->Name== $path) && (($file->Name != '.') && ($file->Name != '..')))
                        {
                            $filePath .= '/'.$path;
                            if($arrayPointer[ $i ] instanceof \FTPClient\DAO\FTPObject)
                                $arrayPointer = &$arrayPointer[ $i ]->Children;
                            break;
                        }
                        $filePath = $this->getActualPath();
                    }
                }

            }
            else if(!empty($rawFile))
            {
                $info = preg_split("/[\s]+/", $rawFile, 9);
                if(($info[8] != '.') && ($info[8] != '..'))
                {
                    $data = array(
                        'text'   => $info[8],
                        'path'   => $filePath . '/'.$info[8],
                        'isDir'  => $info[0]{0} == 'd',
                        'size'   => $this->_byteConvert($info[4]),
                        'chmod'  => $this->_chmodNum($info[0]),
                        'date'   => strtotime($info[5] . ' ' . $info[6] . ' ' . $info[7]),
                        'raw'    => $info
                    );
                    if($data['isDir'])
                        $arrayPointer[] = new FTPDirectory($data['text'],$data['path'],$data['size'],$data['chmod'],$data['date']);
                    else
                        $arrayPointer[] = new FTPFile($data['text'],$data['path'],$data['size'],$data['chmod'],$data['date']);
                }
            }
        }

        return $structure;
    }
    /**
     * Returns file/dir bytes asi pretty string
     *
     * @param $bytes
     * @return string
     */
    protected function _byteConvert($bytes)
    {
        $symbol = array('B', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB');
        $exp = floor( log($bytes) / log(1024) );
        $divisio = pow(1024, floor($exp));
        $byteResult = ($divisio <= 0 ) ? 0 : ($bytes / $divisio);

        return sprintf( '%.2f ' . $symbol[ $exp ], $byteResult);
    }
    /**
     * Returns file/dir permisions as NUMBER
     * @param $chmod
     * @return string
     */
    protected function _chmodNum($chmod)
    {
        $trans = array('-' => '0', 'r' => '4', 'w' => '2', 'x' => '1');
        $chmod = substr(strtr($chmod, $trans), 1);
        $array = str_split($chmod, 3);
        return array_sum(str_split($array[0])) . array_sum(str_split($array[1])) . array_sum(str_split($array[2]));
    }
    /**
     * Close opened connection
     */
    public function close()
    {
        ftp_close($this->_getConnection());
    }
    /**
     * Returns connection resource
     * @return resource
     */
    protected function _getConnection()
    {
        return $this->_connection;
    }
    /**
     * Sets connection resource
     * @param resource $connection
     */
    public function setConnection($connection)
    {
        $this->_connection = $connection;
    }
    /**
     * Returns ftp username for login
     * @return string
     */
    protected function _getUsername()
    {
        return $this->_username;
    }
    /**
     * Setting ftp username for login
     * @param string $username
     */
    public function setUsername($username)
    {
        $this->_username = $username;
    }
    /**
     * Returns ftp password for login
     * @return string
     */
    protected function _getPassword()
    {
        return $this->_password;
    }
    /**
     * Setting ftp password for login
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->_password = $password;
    }
    /**
     * Returns FTP server URL
     * @return string
     */
    protected function _getFtpServer()
    {
        return $this->_ftpServer;
    }
    /**
     * Setting FTP server URL
     * @param string $ftpServer
     */
    public function setFtpServer($ftpServer)
    {
        $this->_ftpServer = $ftpServer;
    }
    /**
     * Returns is FTP  in Passive Mode
     * @return boolean
     */
    public function isPassiveMode()
    {
        return $this->_passive_mode;
    }
    /**
     * Setting FTP connection in Passive Mode
     * @param boolean $passive_mode
     */
    public function setPassiveMode($passive_mode)
    {
        $this->_passive_mode = $passive_mode;
    }

}