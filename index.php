<?php
require_once 'FTPClient/Traits/SortStrategyTrait.php';
require_once 'FTPClient/DAO/FTPObject.php';
require_once 'FTPClient/DAO/FTPFile.php';
require_once 'FTPClient/DAO/FTPDirectory.php';
require_once 'FTPClient/FTPClient.php';
require_once 'Process/CustomProcess.php';

try {
    $process = new \Process\CustomProcess('localhost', 'root', 'a', '/server');
    $files = $process->execute();

}catch (\Exception $e) {
    echo '<h1 style=" background: black; color: red">' . $e->getMessage() . '</h1>';
}
